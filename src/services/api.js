import axios from 'axios';

const api = axios.create({
  baseURL: 'https://systemvet.herokuapp.com/',
  headers: {
    'Authorization': 'bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjAzNzQ4OTQyLCJleHAiOjE2MDM4MzUzNDJ9.lTl5D52fcgi4PBI0NL6ESBFSztS8RP8l7fx_FehiWBg'
  }
})

export default api;