/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.js";

import Usuarios from "views/Usuarios/Usuarios";

import Tutores from "views/Tutores/Tutores"

import Cargo from "views/Cargos/Cargos"
import Area from "views/Area/Area"
import Paciente from 'views/Paciente/'
import Especie from 'views/Especie';
import Cor from 'views/Cor';
import Pelagem from "views/Pelagem/Pelagem";
import Raca from "views/Raca/Raca";
import Fazenda from "views/Fazenda/Fazenda";
import Grupo from "views/Grupo_Produto";
import Produto from "views/Produto";
import Fornecedor from "views/Fornecedor/Fornecedor";

var routes = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin"
  },

  {
    path: "/usuarios",
    name: "Usuarios",
    icon: "ni ni-single-02",
    component: Usuarios,
    layout: "/admin"
  },

  {
    path: "/tutores",
    name: "Tutores",
    icon: "ni ni-circle-08 text-pink",
    component: Tutores,
    layout: "/admin"
  },

  {
    path: "/cargos",
    name: "Cargos",
    icon: "ni ni-bullet-list-67 text-red",
    component: Cargo,
    layout: "/admin"
  },

  {
    path: "/area",
    name: "Area",
    icon: "ni ni-pin-3 text-orange",
    component: Area,
    layout: "/admin"
  },

  {
    path: "/paciente",
    name: "Paciente",
    icon: "ni ni-pin-3 text-orange",
    component: Paciente,
    layout: "/admin"
  },

  {
    path: "/especie",
    name: "Especie",
    icon: "ni ni-pin-3 text-orange",
    component: Especie,
    layout: "/admin"
  },

  {
    path: "/cor",
    name: "Cor",
    icon: "ni ni-pin-3 text-orange",
    component: Cor,
    layout: "/admin"
  },

  {
    path: "/pelagem",
    name: "Pelagem",
    icon: "ni ni-pin-3 text-orange",
    component: Pelagem,
    layout: "/admin"
  },

  {
    path: "/raca",
    name: "Raça",
    icon: "ni ni-pin-3 text-orange",
    component: Raca,
    layout: "/admin"
  },

  {
    path: "/fazenda",
    name: "Fazenda",
    icon: "ni ni-pin-3 text-orange",
    component: Fazenda,
    layout: "/admin"
  },
  {
    path: "/grupo",
    name: "Grupo",
    icon: "ni ni-pin-3 text-orange",
    component: Grupo,
    layout: "/admin"
  },
  {
    path: "/produto",
    name: "Produto",
    icon: "ni ni-pin-3 text-orange",
    component: Produto,
    layout: "/admin"
  },
  {
    path: "/fornecedor",
    name: "Fornecedor",
    icon: "ni ni-pin-3 text-orange",
    component: Fornecedor,
    layout: "/admin"
  },
  {
    path: "/estoque",
    name: "Estoque",
    icon: "ni ni-pin-3 text-orange",
    component: Produto,
    layout: "/admin"
  },
];

export default routes;
