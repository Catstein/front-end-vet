import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

//inputs
import {InputSwitch} from 'primereact/inputswitch';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import {Dialog} from 'primereact/dialog'


import { Button } from 'reactstrap';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';

// import '../clovis/styles.css'

function Cor() {
    const [filter, setFilter] = useState()
    const [nome, setNome] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [showModalConfirm, setShowModalConfirm] = useState(false)
    const [cores, setCores] = useState([])
    const [cor, setCor] = useState({
        id: '',
        nome: '',
        status: true
    })

    async function updateStatus() {
        //chamar api e alterar o status do tutor
        //alterar o estado do componente
    }

    const getCores = async () => {
        await api.get('/cor').then(res => {
            const { data } = res;

            setCores(data.rows);
        })
    }

    useEffect(() => {
        getCores()
    },[])
    
    const openModalForm = (cor = {}) => {
        if (!cor.id) {
            setCor(prev => {return {...prev, id: '', nome: '' , status: true }})
            setShowModalForm(true)
            return
        }
        
        const { id, nome, status } = cor;

        setCor(prev => {return {...prev, id, nome, status: !! (status==='Ativo') }})

        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {
        let msg = (rowData.status==='Ativo') ?
            'Você está desativando a cor selecionada. <br>Gostaria de fazer isso?'
            : 'Você está ativando a cor selecionada. <br>Gostaria de fazer isso?';

        if ((rowData.status === 'Ativo')) {
            Swal.fire({
                title: 'Aviso!',
                html: msg,
                icon: 'warning', 
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Confirmar',
                confirmButtonColor: '#2dce89'
            }).then( async (result) => {
                let response;

                if(result.isConfirmed) {
                    response = await api.put(`/cor/${rowData.id}`, {
                        nome: rowData.nome,
                        status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                    })

                    Swal.fire({
                        title: 'Sucesso!',
                        text: 'Status da cor atualizada com sucesso.',
                        icon: 'success'
                    }).then(() => {
                        getCores();
                    })
                }

                // setShowModalForm(false);
                // getCargos();
            })
        }      
    } 
    
    const closeModal = () => {
        setShowModalForm(false)
        setShowModalConfirm(false)
        setNome('')
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
    ];

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} 
            header={col.header}/>;
    });

    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} 
                  tooltip={(rowData.status === 'Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                  openStatusHandler(rowData);
                }} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : 
            {'inactive' : true};
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (cor.id === '') {
            response = await api.post(`/cor`, {
                nome: data.nome,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Espécie cadastrada com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getCores();
            })
        } else {
            response = await api.put(`/cor/${cor.id}`, {
                nome: data.nome,
                status: data.status ? 'Ativo' : 'Inativo'
            })
            
            Swal.fire({
                title: 'Sucesso!',
                text: 'Espécie atualizada com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getCores();
            })
        }
        
    };

    console.log('watchAllFields', watchAllFields)

    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de Espécies</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar Cor
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp;
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>

                <div className="col text-right">
                    <InputText type="search" onInput={(e) => 
                        setFilter(e.target.value)} 
                        placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={cores} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há espécies cadastradas"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink">
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} 
                                    style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog 
                header="Adicionando Cor" 
                maximizable={false} 
                visible={showModalForm} 
                style={{ width: '40vw' }} 
                onHide={closeModal}
            >
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" 
                            ref={register} defaultValue={cor.nome}/>
                        </div>
                    </div>

                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" 
                        ref={register} checked={cor.status}/>
                        <label className="form-check-label" for="check">Ativo?</label>
                    </div>

                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>

                </form>
            </Dialog>

            <Dialog header="Deseja desativar a cor selecionada?" visible={showModalConfirm} 
              style={{ width: '20vw' }} modal onHide={closeModal}>
                <div className="text-center">
                    <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                    <Button className="btn btn-success">
                        Confirmar
                    </Button>
                </div>
            </Dialog>

            <Dialog header="Deseja desativar a cor?" visible={showModalConfirm} style={{ width: '20vw' }} onHide={closeModal}>
                <div className="text-center">
                    <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                    <Button 
                        className="btn btn-success" 
                        onClick={updateStatus}
                    >
                        Confirmar
                    </Button>
                </div>
            </Dialog>
        </>
    );
}

export default Cor;