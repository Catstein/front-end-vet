import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

//inputs
import {InputSwitch} from 'primereact/inputswitch';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import {Dialog} from 'primereact/dialog'


import { Button } from 'reactstrap';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';

function Fazenda() {

    const [filter, setFilter] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [showModalConfirm, setShowModalConfirm] = useState(false)
    const [estados, setEstados] = useState([])
    const [fazendas, setFazendas] = useState([])
    const [fazenda, setFazenda] = useState({
        id: '',
        nome: '',
        telefone: '',
        email: '',
        cep: '',
        endereco: '',
        numero: '',
        complemento: '',
        ponto_referencia: '',
        bairro: '',
        cidade: '',
        estado: '',
        status: true
    })

    const getEstados = async() => {
        const url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
        let response = await fetch(url);
        let data = await response.json();
        setEstados(data);
    }


    const getFazendas = async () => {
        await api.get('/fazenda').then(res => {
            const { data } = res;
            setFazendas(data.rows);
        })
    }


    useEffect(() => {
        getFazendas()
        getEstados()
    },[])
    
    const openModalForm = (fazenda = {}) => {
        if (!fazenda.id) {
            setFazenda(prev => {return {
                ...prev, 
                id: '', 
                nome: '', 
                telefone: '',
                email: '',
                cep: '',
                endereco: '',
                numero: '',
                complemento: '',
                ponto_referencia: '',
                bairro: '',
                cidade: '',
                estado: '',
                status: true 
            }})
            setShowModalForm(true)
            return
        }
        
        const { 
            id, 
            nome,
            telefone,
            email,
            cep,
            endereco,
            numero,
            complemento,
            ponto_referencia,
            bairro,
            cidade,
            estado, 
            status 
        } = fazenda;

        setFazenda(prev => {return {
            ...prev, 
            id, 
            nome, 
            telefone,
            email,
            cep,
            endereco,
            numero,
            complemento,
            ponto_referencia,
            bairro,
            cidade,
            estado, 
            status: !!(status==='Ativo') }})


        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {

        let msg = (rowData.status==='Ativo') ?
            'Você está desativando a fazenda. <br>Gostaria de fazer isso?'
            : 'Você está ativando a fazenda. <br>Gostaria de fazer isso?';

        Swal.fire({
            title: 'Aviso!',
            html: msg,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            cancelButtonColor: '#dc3545',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#2dce89'
        }).then( async (result) => {

            let response;

            if(result.isConfirmed) {
                response = await api.put(`/fazenda/${rowData.id}`, {
                    nome: rowData.nome,
                    telefone: rowData.telefone,
                    email: rowData.email,
                    cep: rowData.cep,
                    endereco: rowData.endereco,
                    numero: rowData.numero,
                    complemento: rowData.complemento,
                    ponto_referencia: rowData.ponto_referencia,
                    bairro: rowData.bairro,
                    cidade: rowData.cidade,
                    estado: rowData.estado,
                    status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                })
        
                Swal.fire({
                    title: 'Sucesso!',
                    text: 'Status da fazenda atualizado com sucesso.',
                    icon: 'success'
                }).then(() => {
                    getFazendas();
                })
            }
        })
        
    } 

    const closeModal = () => {
        setShowModalForm(false)
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
        {field: 'telefone', header: 'Telefone', sortable: 1},
        {field: 'cidade', header: 'Cidade', sortable: 1},
        {field: 'estado', header: 'Estado', sortable: 1},
    ];

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} 
            header={col.header}/>;
    });

    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} tooltip={(rowData.status==='Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                    openStatusHandler(rowData);
                }} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : 
            {'inactive' : true};
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (fazenda.id === '') {
            response = await api.post(`/fazenda`, {
                nome: data.nome,
                telefone: data.telefone,
                email: data.email,
                cep: data.cep,
                endereco: data.endereco,
                numero: data.numero,
                complemento: data.complemento,
                ponto_referencia: data.ponto_referencia,
                bairro: data.bairro,
                cidade: data.cidade,
                estado: data.estado,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Fazenda cadastrada com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getFazendas();
            })
        } else {
            response = await api.put(`/fazenda/${fazenda.id}`, {
                nome: data.nome,
                telefone: data.telefone,
                email: data.email,
                cep: data.cep,
                endereco: data.endereco,
                numero: data.numero,
                complemento: data.complemento,
                ponto_referencia: data.ponto_referencia,
                bairro: data.bairro,
                cidade: data.cidade,
                estado: data.estado,
                status: data.status ? 'Ativo' : 'Inativo'
            })

    
            Swal.fire({
                title: 'Sucesso!',
                text: 'Fazenda atualizada com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getFazendas();
            })
        }
        
    };


    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de fazendas</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar fazenda
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp;
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>

                <div className="col text-right">
                    <InputText type="search" onInput={(e) => 
                        setFilter(e.target.value)} 
                        placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={fazendas} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há fazendas cadastradas"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink">
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} 
                                    style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog 
                header="Adicionando fazenda" 
                maximizable={false} 
                visible={showModalForm} 
                style={{ width: '40vw' }} 
                onHide={closeModal}
            >
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="nome" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" ref={register} defaultValue={fazenda.nome}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-6">
                            <label htmlFor="telefone" className="form-control-label">Telefone</label>
                            <input type="text" className="form-control" placeholder="telefone" name="telefone" ref={register} defaultValue={fazenda.telefone}/>
                        </div>
                        
                        <div className="col-md-6">
                            <label htmlFor="email" className="form-control-label">Email</label>
                            <input type="text" className="form-control" placeholder="email" name="email" ref={register} defaultValue={fazenda.email}/>
                        </div>

                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-3">
                            <label htmlFor="cep" className="form-control-label">CEP</label>
                            <input type="text" className="form-control" placeholder="cep" name="cep" ref={register} defaultValue={fazenda.cep}/>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="endereco" className="form-control-label">Endereço</label>
                            <input type="text" className="form-control" placeholder="endereco" name="endereco" ref={register} defaultValue={fazenda.endereco}/>
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="numero" className="form-control-label">Número</label>
                            <input type="text" className="form-control" placeholder="numero" name="numero" ref={register} defaultValue={fazenda.numero}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-6">
                            <label htmlFor="complemento" className="form-control-label">Complemento</label>
                            <input type="text" className="form-control" placeholder="complemento" name="complemento" ref={register} defaultValue={fazenda.complemento}/>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="ponto_referencia" className="form-control-label">Ponto de referência</label>
                            <input type="text" className="form-control" placeholder="ponto de referência" name="ponto_referencia" ref={register} defaultValue={fazenda.ponto_referencia}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-4">
                            <label htmlFor="bairro" className="form-control-label">Bairro</label>
                            <input type="text" className="form-control" placeholder="bairro" name="bairro" ref={register} defaultValue={fazenda.bairro}/>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="cidade" className="form-control-label">Cidade</label>
                            <input type="text" className="form-control" placeholder="cidade" name="cidade" ref={register} defaultValue={fazenda.cidade}/>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="estado" className="form-control-label">Estado</label>
                            <select name="estado" className="form-control" name="estado" id="estado" ref={register} defaultValue={fazenda.estado}>
                                <option value="" key="0" disabled selected>Selecione o estado</option>
                                {
                                    estados.map((estado) => {
                                        return <option key={estado.sigla} value={estado.sigla}>{estado.nome}</option>
                                    })
                                }
                            </select>
                        </div>
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" ref={register} checked={fazenda.status}/>
                        <label className="form-check-label" for="check">Ativo?</label>
                    </div>
                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>
                </form>
            </Dialog>
        </>
    );
}

export default Fazenda