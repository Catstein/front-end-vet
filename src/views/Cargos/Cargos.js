import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

import { Dialog } from 'primereact/dialog' 

import Swal from 'sweetalert2';

//inputs
import {InputSwitch} from 'primereact/inputswitch' 
import { InputText } from 'primereact/inputtext' 
import { Dropdown } from 'primereact/dropdown' 


import { Button } from 'reactstrap' 

import { useForm } from "react-hook-form";

function Cargo() {

    const [cargos, setCargos] = useState([])
    const [filter, setFilter] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [showModalConfirm, setShowModalConfirm] = useState(false)
    const [nome, setNome] = useState()
    const [cargo, setCargo] = useState({
        id: '',
        nome: '',
        descricao: '',
        status: true
    })

    async function updateStatus() {
        //chamar api e alterar o status do tutor
        //alterar o estado do componente
    }

    async function store() {
        alert('salvar')
        /*if(name && descricao) {
            api.post('/cargos', {
                nome,
                descricao
            }).then(() => {
               alert('Cadastro realizado com sucesso!') 
            }).catch(() => {
                alert('Erro no cadastro')
            })
        }*/
    }

    async function update() {
        alert('atualizar')
        /*if(name && descricao) {
            api.post('/cargos', {
                nome,
                descricao
            }).then(() => {
               alert('Cadastro realizado com sucesso!') 
            }).catch(() => {
                alert('Erro no cadastro')
            })
        }*/
    }

    const getCargos = async () => {
        console.log('teste')
        await api.get('/cargo').then(res => {
            console.log('res', res)
            const { data } = res;
    
            console.log('lista de cargos:', data);
    
            setCargos(data.rows);
        })
    }

    useEffect(() => {
        getCargos()
    },[])

    const openModalForm = (cargo = {}) => {
        if (!cargo.id) {
            setCargo(prev => {return {...prev, id: '', nome: '' ,descricao: '', status: true }})
            setShowModalForm(true)
            return
        }
        
        const { id, nome, descricao, status } = cargo;
        console.log('tutor:', cargo)

        setCargo(prev => {return {...prev, id, nome ,descricao, status: !!(status==='Ativo') }})

        console.log('cargo alterado?', cargo)

        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {
        console.log('cargo atual', rowData)

        let msg = (rowData.status==='Ativo') ?
            'Você está desativando o cargo selecionado. <br>Gostaria de fazer isso?'
            : 'Você está ativando o cargo selecionado. <br>Gostaria de fazer isso?';

            Swal.fire({
                title: 'Aviso!',
                html: msg,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Confirmar',
                confirmButtonColor: '#2dce89'
            }).then( async (result) => {
                console.log('res', result);

                let response;

                if(result.isConfirmed) {
                    response = await api.put(`/cargo/${rowData.id}`, {
                        nome: rowData.nome,
                        descricao: rowData.descricao,
                        status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                    })
        
                    console.log('res', response);
            
                    Swal.fire({
                        title: 'Sucesso!',
                        text: 'Status do Cargo atualizado com sucesso.',
                        icon: 'success'
                    }).then(() => {
                        getCargos();
                    })
                }

                // setShowModalForm(false);
                // getCargos();
            })
        
    } 

    const closeModal = () => {
        setShowModalForm(false)
        setShowModalConfirm(false)
        setNome('')
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
        {field: 'descricao', header: 'Descricao', sortable: 1}
    ] 

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} header={col.header}/>
    })

    const footerModalForm = (
        <div>
            <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
            <Button 
                className="btn btn-success" 
                onClick={closeModal}
            >
                {nome ? 'Atualizar' : 'Salvar'}
            </Button>
        </div>
    ) 

    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} tooltip={(rowData.status==='Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                    openStatusHandler(rowData);
                }} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : {'inactive' : true} 
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (cargo.id === '') {
            response = await api.post(`/cargo`, {
                nome: data.nome,
                descricao: data.descricao,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Cargo cadastrado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getCargos();
            })
        } else {
            response = await api.put(`/cargo/${cargo.id}`, {
                nome: data.nome,
                descricao: data.descricao,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            console.log('res', response);
    
            Swal.fire({
                title: 'Sucesso!',
                text: 'Cargo atualizado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getCargos();
            })
        }


                
        console.log('teste', data)
        console.log('response', response);
        
    };

    console.log('watchAllFields', watchAllFields)

    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de cargos</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar cargo
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp; 
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>
                <div className="col text-right">
                    <InputText type="search" onInput={(e) => setFilter(e.target.value)} placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={cargos} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há cargos cadastrados"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink"
                            >
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog header="Adicionando cargo" maximizable={false} visible={showModalForm} style={{ width: '40vw' }} onHide={closeModal}>
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" ref={register} defaultValue={cargo.nome}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">Descrição</label>
                            <input type="text" className="form-control" placeholder="Descrição" name="descricao" ref={register} defaultValue={cargo.descricao}/>
                        </div>
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" ref={register} checked={cargo.status}/>
                        <label className="form-check-label" for="check">Ativo?</label>
                    </div>
                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>
                </form>
                
            </Dialog>

            <Dialog header="Deseja desativar o cargo?" visible={showModalConfirm} style={{ width: '20vw' }} onHide={closeModal}>
                <div className="text-center">
                    <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                    <Button 
                        className="btn btn-success" 
                        onClick={updateStatus}
                    >
                        Confirmar
                    </Button>
                </div>
            </Dialog>
        </>
    )
}

export default Cargo