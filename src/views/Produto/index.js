import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

import {InputSwitch} from 'primereact/inputswitch';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import {Dialog} from 'primereact/dialog'


import { Button } from 'reactstrap';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';


function Produto() {
    const [filter, setFilter] = useState()
    const [descricao, setNome] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [showModalConfirm, setShowModalConfirm] = useState(false)
    const [grupos, setGrupos] = useState([])
    const [produtos, setProdutos] = useState([])
    const [produto, setProduto] = useState({
        id: '',
        descricao: '',
        preco_custo: '',
        grupo_id: '',
        vacina: '',
        status: true
    })

    async function updateStatus() {
        //chamar api e alterar o status do tutor
        //alterar o estado do componente
    }

    const getGrupos = async () => {
      await api.get('/grupo').then(res => {
          const { data } = res;
          setGrupos(data.rows);
      })
    }

    const getProdutos = async () => {
        await api.get('/produto').then(res => {
            const { data } = res;
            setProdutos(data.rows);
        })
    }

    useEffect(() => {
        getProdutos();
        getGrupos();
    },[])
    
    const openModalForm = (produto = {}) => {
        if (!produto.id) {
            setProduto(prev => {return {
              ...prev, id: '', 
              descricao: '', 
              preco_custo: '', 
              grupo_id: '', 
              vacina: '', 
              status: true 
            }})

            setShowModalForm(true)
            return
        }
        
        const { id, descricao, preco_custo, grupo_id, vacina, status } = produto;

        setProduto(prev => {return {
          ...prev, 
          id, 
          descricao, 
          preco_custo, 
          grupo_id, 
          vacina, 
          status: !! (status==='Ativo') 
        }})

        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {
        let msg = (rowData.status==='Ativo') ?
            'Você está desativando o produto selecionado. <br>Gostaria de fazer isso?'
            : 'Você está ativando o produto selecionado. <br>Gostaria de fazer isso?';

            Swal.fire({
                title: 'Aviso!',
                html: msg,
                icon: 'warning', 
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Confirmar',
                confirmButtonColor: '#2dce89'
            }).then( async (result) => {
                let response;

                if(result.isConfirmed) {
                    response = await api.put(`/produto/${rowData.id}`, {
                        descricao: rowData.descricao,
                        preco_custo: rowData.preco_custo,
                        grupo_id: rowData.grupo_id,
                        vacina: rowData.vacina,
                        status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                    })

                    Swal.fire({
                        title: 'Sucesso!',
                        text: 'Status da produto atualizado com sucesso.',
                        icon: 'success'
                    }).then(() => {
                        getProdutos();
                    })
                }

                // setShowModalForm(false);
                // getCargos();
            })
             
    } 
    
    const closeModal = () => {
        setShowModalForm(false)
        setShowModalConfirm(false)
    }

    const cols = [
        {field: 'descricao', header: 'Descrição', sortable: 1},
        {field: 'grupo_id', header: 'Grupo', sortable: 1},
        {field: 'vacina', header: 'Vacina', sortable: 1},
    ];

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} 
            header={col.header}/>;
    });

    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} 
                  tooltip={(rowData.status === 'Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                  openStatusHandler(rowData);
                }} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : 
            {'inactive' : true};
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (produto.id === '') {
            response = await api.post(`/produto`, {
                descricao: data.descricao,
                preco_custo: data.preco_custo,
                grupo_id: data.grupo_id,
                vacina: data.vacina,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Produto cadastrado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getProdutos();
            })
        } else {
            response = await api.put(`/produto/${produto.id}`, {
                descricao: data.descricao,
                preco_custo: data.preco_custo,
                grupo_id: data.grupo_id,
                vacina: data.vacina,
                status: data.status ? 'Ativo' : 'Inativo'
            })
            
            Swal.fire({
                title: 'Sucesso!',
                text: 'Produto atualizado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getProdutos();
            })
        }
        
    };

    console.log('watchAllFields', watchAllFields)

    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de Produtos</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar Produto
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp;
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>

                <div className="col text-right">
                    <InputText type="search" onInput={(e) => 
                        setFilter(e.target.value)} 
                        placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={produtos} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há produtos cadastrados"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink">
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} 
                                    style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog 
                header="Adicionando Produto" 
                maximizable={false} 
                visible={showModalForm} 
                style={{ width: '40vw' }} 
                onHide={closeModal}
            >
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-10">
                            <label htmlFor="" className="form-control-label">Descrição</label>
                            <input type="text" className="form-control" placeholder="Descrição" name="descricao" 
                            ref={register} defaultValue={produto.descricao}/>
                        </div>

                        <div className="col-md-2">
                            <label htmlFor="" className="form-control-label">Preço</label>
                            <input type="number" className="form-control" placeholder="Preço" name="preco" 
                            ref={register} defaultValue={produto.preco_custo}/>
                        </div>
                    </div>

                    
                    <div className="form-row mb-3">
                        <div className="col-md-6">
                            <label htmlFor="grupo" className="form-control-label">Grupo</label>

                            <select name="grupo" className="form-control" name="grupo" id="grupo" ref={register} defaultValue={produto.grupo_id}>
                                <option value="" key="0" disabled selected>Selecione o grupo</option>
                                    {
                                        grupos.length > 0 &&
                                        grupos.map((grupo) => <option key={grupo.id} value={grupo.id}>{grupo.nome}</option> )
                                    }
                            </select>
                        </div>

                        <div className="col-md-6">
                            <label htmlFor="" className="form-control-label">Vacina</label>
                            <input type="text" className="form-control" placeholder="Vacina" name="Vacina" 
                            ref={register} defaultValue={produto.vacina}/>
                        </div>
                    </div>

                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" 
                        ref={register} checked={produto.status}/>
                        <label className="form-check-label" for="check">Ativo?</label>
                    </div>

                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>

                </form>
            </Dialog>

            <Dialog header="Deseja desativar o produto selecionado?" visible={showModalConfirm} 
              style={{ width: '20vw' }} modal onHide={closeModal}>
                <div className="text-center">
                    <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                    <Button className="btn btn-success">
                        Confirmar
                    </Button>
                </div>
            </Dialog>

            <Dialog header="Deseja desativar o produto?" visible={showModalConfirm} style={{ width: '20vw' }} onHide={closeModal}>
                <div className="text-center">
                    <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                    <Button 
                        className="btn btn-success" 
                        onClick={updateStatus}
                    >
                        Confirmar
                    </Button>
                </div>
            </Dialog>
        </>
    );
}

export default Produto;