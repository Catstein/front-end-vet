import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

import { Dialog } from 'primereact/dialog' 

import Swal from 'sweetalert2';

//inputs
import {InputSwitch} from 'primereact/inputswitch' 
import { InputText } from 'primereact/inputtext' 
import { Dropdown } from 'primereact/dropdown' 


import { Button } from 'reactstrap' 

import { useForm } from "react-hook-form";

function Fornecedor() {

    const [fornecedores, setFornecedores] = useState([])
    const [filter, setFilter] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [nome, setNome] = useState()
    const [estados, setEstados] = useState([])
    const [fornecedor, setFornecedor] = useState({
      nome: '',
      tipo_pessoa: '',
      cpf_cnpj: '',
      rg_ie: '',
      cep: '',
      endereco: '',
      numero: '',
      complemento: '',
      ponto_referencia: '',
      bairro: '',
      cidade: '',
      estado: '',
      observacao: '',
      contribuinte: '',
      status: true
    })

    const getEstados = async() => {
      const url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
      let response = await fetch(url);
      let data = await response.json();
      setEstados(data);
    }

    const getFornecedores = async () => {
        await api.get('/fornecedor').then(res => {
            const { data } = res;
            console.log('lista de fornecedores:', data.rows);
            setFornecedores(data.rows);
        })
    }

    useEffect(() => {
        getFornecedores()
        getEstados()
    },[])

    const openModalForm = (fornecedor = {}) => {
        if (!fornecedor.id) {
            setFornecedor(prev => {
              return {
                ...prev,
                id: '',
                nome: '',
                tipo_pessoa: '',
                cpf_cnpj: '',
                rg_ie: '',
                cep: '',
                endereco: '',
                numero: '',
                complemento: '',
                ponto_referencia: '',
                bairro: '',
                cidade: '',
                estado: '',
                observacao: '',
                contribuinte: '',
                status: true 
              }
            })
            setShowModalForm(true)
            return
        }
        
        const { id, nome, tipo_pessoa, cpf_cnpj, rg_ie, cep, endereco, numero, complemento, ponto_referencia, bairro, cidade, estado, observacao, contribuinte, status } = fornecedor;
        console.log('tutor:', fornecedor)

        setFornecedor(prev => {
          return {
            ...prev,
            id: id,
            nome: nome,
            tipo_pessoa: tipo_pessoa,
            cpf_cnpj: cpf_cnpj,
            rg_ie: rg_ie,
            cep: cep,
            endereco: endereco,
            numero: numero,
            complemento: complemento,
            ponto_referencia: ponto_referencia,
            bairro: bairro,
            cidade: cidade,
            estado: estado,
            observacao: observacao,
            contribuinte: contribuinte,
            status: !!(status==='Ativo') 
          }
        })

        console.log('fornecedor alterado?', fornecedor)

        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {
        console.log('fornecedor atual', rowData)

        let msg = (rowData.status==='Ativo') ?
            'Você está desativando o fornecedor selecionado. <br>Gostaria de fazer isso?'
            : 'Você está ativando o fornecedor selecionado. <br>Gostaria de fazer isso?';

            Swal.fire({
                title: 'Aviso!',
                html: msg,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Confirmar',
                confirmButtonColor: '#2dce89'
            }).then( async (result) => {
                console.log('res', result);

                let response;

                if(result.isConfirmed) {
                    response = await api.put(`/fornecedor/${rowData.id}`, {
                        nome: rowData.nome,
                        tipo_pessoa: rowData.tipo_pessoa,
                        cpf_cnpj: rowData.cpf_cnpj,
                        rg_ie: rowData.rg_ie,
                        cep: rowData.cep,
                        endereco: rowData.endereco,
                        numero: rowData.numero,
                        complemento: rowData.complemento,
                        ponto_referencia: rowData.ponto_referencia,
                        bairro: rowData.bairro,
                        cidade: rowData.cidade,
                        estado: rowData.estado,
                        observacao: rowData.observacao,
                        contribuinte: rowData.contribuinte,
                        status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                    })
        
                    console.log('res', response);
            
                    Swal.fire({
                        title: 'Sucesso!',
                        text: 'Status do Fornecedor atualizado com sucesso.',
                        icon: 'success'
                    }).then(() => {
                        getFornecedores();
                    })
                }

                // setShowModalForm(false);
                // getCargos();
            })
        
    } 

    const closeModal = () => {
        setShowModalForm(false)
        setNome('')
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
        {field: 'cpf_cnpj', header: 'CPF/CNPJ', sortable: 1},
        {field: 'rg_ie', header: 'RG/IE', sortable: 1}
    ] 

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} header={col.header}/>
    })

    const footerModalForm = (
        <div>
            <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
            <Button 
                className="btn btn-success" 
                onClick={closeModal}
            >
                {nome ? 'Atualizar' : 'Salvar'}
            </Button>
        </div>
    ) 

    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} tooltip={(rowData.status==='Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                    openStatusHandler(rowData);
                }} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : {'inactive' : true} 
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (fornecedor.id === '') {
            response = await api.post(`/fornecedor`, {
              id: data.id,
              nome: data.nome,
              tipo_pessoa: data.tipo_pessoa,
              cpf_cnpj: data.cpf_cnpj,
              rg_ie: data.rg_ie,
              cep: data.cep,
              endereco: data.endereco,
              numero: data.numero,
              complemento: data.complemento,
              ponto_referencia: data.ponto_referencia,
              bairro: data.bairro,
              cidade: data.cidade,
              estado: data.estado,
              observacao: data.observacao,
              contribuinte: data.contribuinte,
              status: data.status ? 'Ativo' : 'Inativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Cargo cadastrado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getFornecedores();
            })
        } else {
            response = await api.put(`/fornecedor/${fornecedor.id}`, {
              nome: data.nome,
              tipo_pessoa: data.tipo_pessoa,
              cpf_cnpj: data.cpf_cnpj,
              rg_ie: data.rg_ie,
              cep: data.cep,
              endereco: data.endereco,
              numero: data.numero,
              complemento: data.complemento,
              ponto_referencia: data.ponto_referencia,
              bairro: data.bairro,
              cidade: data.cidade,
              estado: data.estado,
              observacao: data.observacao,
              contribuinte: data.contribuinte,
              status: data.status ? 'Ativo' : 'Inativo'
            })

            console.log('res', response);
    
            Swal.fire({
                title: 'Sucesso!',
                text: 'Cargo atualizado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getFornecedores();
            })
        }


                
        console.log('teste', data)
        console.log('response', response);
        
    };

    console.log('watchAllFields', watchAllFields)

    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de fornecedores</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar fornecedor
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp; 
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>
                <div className="col text-right">
                    <InputText type="search" onInput={(e) => setFilter(e.target.value)} placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={fornecedores}
                                paginator={true}
                                rows={rows}
                                responsive={true}
                                header={headerTable}
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há cargos cadastrados"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink"
                            >
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog header="Adicionando fornecedor" maximizable={false} visible={showModalForm} style={{ width: '40vw' }} onHide={closeModal}>
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" ref={register} defaultValue={fornecedor.nome}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                      <div className="col-md-6">

                        <label htmlFor="tipo_pessoa" className="form-control-label">Tipo de pessoa</label>
                        <select name="tipo_pessoa" className="form-control" name="tipo_pessoa" id="tipo_pessoa" ref={register} defaultValue={fornecedor.tipo_pessoa}>
                            <option value="" key="0" disabled selected>Selecione o tipo de pessoa</option>
                            <option value="Fisica" key="1" >Física</option>
                            <option value="Juridica" key="2" >Juridíca</option>
                        </select>
                      </div>
                      <div className="col-md-6">
                          <label htmlFor="cpf_cnpj" className="form-control-label">CPF/CNPJ</label>
                          <input type="text" className="form-control" placeholder="Digite o Documento" name="cpf_cnpj" ref={register} defaultValue={fornecedor.cpf_cnpj}/>
                      </div>
                      
                    </div>
                    <div className="form-row mb-3">
                      <div className="col-md-6">
                          <label htmlFor="rg_ie" className="form-control-label">RG/IE</label>
                          <input type="text" className="form-control" placeholder="Digite o Documento" name="rg_ie" ref={register} defaultValue={fornecedor.rg_ie}/>
                      </div>
                      <div className="col-md-6">
                          <label htmlFor="contribuinte" className="form-control-label">Contribuinte</label>
                          <select name="contribuinte" className="form-control" name="contribuinte" id="contribuinte" ref={register} defaultValue={fornecedor.contribuinte}>
                              <option value="" disabled selected>Selecione um contribuinte</option>
                              <option value="Sim">Sim</option>
                              <option value="Não">Não</option>
                          </select>
                      </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">Observação</label>
                            <input type="text" className="form-control" placeholder="Observação" name="observacao" ref={register} defaultValue={fornecedor.observacao}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-3">
                            <label htmlFor="cep" className="form-control-label">CEP</label>
                            <input type="text" className="form-control" placeholder="cep" name="cep" ref={register} defaultValue={fornecedor.cep}/>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="endereco" className="form-control-label">Endereço</label>
                            <input type="text" className="form-control" placeholder="endereco" name="endereco" ref={register} defaultValue={fornecedor.endereco}/>
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="numero" className="form-control-label">Número</label>
                            <input type="text" className="form-control" placeholder="numero" name="numero" ref={register} defaultValue={fornecedor.numero}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-6">
                            <label htmlFor="complemento" className="form-control-label">Complemento</label>
                            <input type="text" className="form-control" placeholder="complemento" name="complemento" ref={register} defaultValue={fornecedor.complemento}/>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="ponto_referencia" className="form-control-label">Ponto de referência</label>
                            <input type="text" className="form-control" placeholder="ponto de referência" name="ponto_referencia" ref={register} defaultValue={fornecedor.ponto_referencia}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-4">
                            <label htmlFor="bairro" className="form-control-label">Bairro</label>
                            <input type="text" className="form-control" placeholder="bairro" name="bairro" ref={register} defaultValue={fornecedor.bairro}/>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="cidade" className="form-control-label">Cidade</label>
                            <input type="text" className="form-control" placeholder="cidade" name="cidade" ref={register} defaultValue={fornecedor.cidade}/>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="estado" className="form-control-label">Estado</label>
                            <select name="estado" className="form-control" name="estado" id="estado" ref={register} defaultValue={fornecedor.estado}>
                                <option value="" key="0" disabled selected>Selecione o estado</option>
                                {
                                    estados.map((estado) => {
                                        return <option key={estado.sigla} value={estado.sigla}>{estado.nome}</option>
                                    })
                                }
                            </select>
                        </div>
                    </div>

                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" ref={register} checked={fornecedor.status}/>
                        <label className="form-check-label" htmlFor="check">Ativo?</label>
                    </div>
                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>
                </form>
                
            </Dialog>

        </>
    )
}

export default Fornecedor