import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

//inputs
import {InputSwitch} from 'primereact/inputswitch';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import {Dialog} from 'primereact/dialog'


import { Button } from 'reactstrap';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';

function Tutor() {

    const [filter, setFilter] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [showModalConfirm, setShowModalConfirm] = useState(false)
    const [estados, setEstados] = useState([])
    const [tutores, setTutores] = useState([])
    const [tutor, setTutor] = useState({
        id: '',
        nome: '',
        tipo_pessoa: '',
        cpf_cnpj: '',
        rg_ie: '',
        nacionalidade: '',
        sexo: '',
        data_nascimento: '',
        cep: '',
        endereco: '',
        numero: '',
        complemento: '',
        ponto_referencia: '',
        bairro: '',
        cidade: '',
        estado: '',
        observacao: '',
        status: true
    })

    const getEstados = async() => {
        const url = 'https://servicodados.ibge.gov.br/api/v1/localidades/estados'
        let response = await fetch(url);
        let data = await response.json();
        setEstados(data);
    }


    const getTutores = async () => {
        await api.get('/tutor').then(res => {
            const { data } = res;
            setTutores(data.rows);
        })
    }


    useEffect(() => {
        getTutores()
        getEstados()
    },[])
    
    const openModalForm = (tutor = {}) => {
        if (!tutor.id) {
            setTutor(prev => {return {
                ...prev, 
                id: '',
                nome: '',
                tipo_pessoa: '',
                cpf_cnpj: '',
                rg_ie: '',
                nacionalidade: '',
                sexo: '',
                data_nascimento: '',
                cep: '',
                endereco: '',
                numero: '',
                complemento: '',
                ponto_referencia: '',
                bairro: '',
                cidade: '',
                estado: '',
                observacao: '',
                status: true
            }})
            setShowModalForm(true)
            return
        }
        
        const { 
            id,
            nome,
            tipo_pessoa,
            cpf_cnpj,
            rg_ie,
            nacionalidade,
            sexo,
            data_nascimento,
            cep,
            endereco,
            numero,
            complemento,
            ponto_referencia,
            bairro,
            cidade,
            estado,
            observacao,
            status
        } = tutor;

        setTutor(prev => {return {
            ...prev, 
            id,
            nome,
            tipo_pessoa,
            cpf_cnpj,
            rg_ie,
            nacionalidade,
            sexo,
            data_nascimento,
            cep,
            endereco,
            numero,
            complemento,
            ponto_referencia,
            bairro,
            cidade,
            estado,
            observacao,
            status: !!(status==='Ativo') }})


        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {

        let msg = (rowData.status==='Ativo') ?
            'Você está desativando a tutor. <br>Gostaria de fazer isso?'
            : 'Você está ativando a tutor. <br>Gostaria de fazer isso?';

        Swal.fire({
            title: 'Aviso!',
            html: msg,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            cancelButtonColor: '#dc3545',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#2dce89'
        }).then( async (result) => {

            let response;

            if(result.isConfirmed) {
                response = await api.put(`/tutor/${rowData.id}`, {
                    nome: rowData.nome,
                    tipo_pessoa: rowData.tipo_pessoa,
                    cpf_cnpj: rowData.cpf_cnpj,
                    rg_ie: rowData.rg_ie,
                    nacionalidade: rowData.nacionalidade,
                    sexo: rowData.sexo,
                    data_nascimento: rowData.data_nascimento,
                    cep: rowData.cep,
                    endereco: rowData.endereco,
                    numero: rowData.numero,
                    complemento: rowData.complemento,
                    ponto_referencia: rowData.ponto_referencia,
                    bairro: rowData.bairro,
                    cidade: rowData.cidade,
                    estado: rowData.estado,
                    observacao: rowData.observacao,
                    status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                })
        
                Swal.fire({
                    title: 'Sucesso!',
                    text: 'Status da tutor atualizado com sucesso.',
                    icon: 'success'
                }).then(() => {
                    getTutores();
                })
            }
        })
        
    } 

    const closeModal = () => {
        setShowModalForm(false)
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
        {field: 'tipo_pessoa', header: 'Tipo pessoa', sortable: 1},
        {field: 'cpf_cnpj', header: 'CPF/CNPJ', sortable: 1},
        {field: 'rg_ie', header: 'RG/IE', sortable: 1},
    ];

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} 
            header={col.header}/>;
    });

    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} tooltip={(rowData.status==='Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                    openStatusHandler(rowData);
                }} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : 
            {'inactive' : true};
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (tutor.id === '') {
            response = await api.post(`/tutor`, {
                nome: data.nome,
                tipo_pessoa: data.tipo_pessoa,
                cpf_cnpj: data.cpf_cnpj,
                rg_ie: data.rg_ie,
                nacionalidade: data.nacionalidade,
                sexo: data.sexo,
                data_nascimento: data.data_nascimento,
                cep: data.cep,
                endereco: data.endereco,
                numero: data.numero,
                complemento: data.complemento,
                ponto_referencia: data.ponto_referencia,
                bairro: data.bairro,
                cidade: data.cidade,
                estado: data.estado,
                observacao: data.observacao,
                status: (data.status==='Ativo') ? 'Inativo' : 'Ativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Tutor cadastrada com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getTutores();
            })
        } else {
            response = await api.put(`/tutor/${tutor.id}`, {
                nome: data.nome,
                tipo_pessoa: data.tipo_pessoa,
                cpf_cnpj: data.cpf_cnpj,
                rg_ie: data.rg_ie,
                nacionalidade: data.nacionalidade,
                sexo: data.sexo,
                data_nascimento: data.data_nascimento,
                cep: data.cep,
                endereco: data.endereco,
                numero: data.numero,
                complemento: data.complemento,
                ponto_referencia: data.ponto_referencia,
                bairro: data.bairro,
                cidade: data.cidade,
                estado: data.estado,
                observacao: data.observacao,
                status: (data.status==='Ativo') ? 'Inativo' : 'Ativo'
            })

    
            Swal.fire({
                title: 'Sucesso!',
                text: 'Tutor atualizado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getTutores();
            })
        }
        
    };


    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de tutores</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar tutor
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp;
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>

                <div className="col text-right">
                    <InputText type="search" onInput={(e) => 
                        setFilter(e.target.value)} 
                        placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={tutores} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há tutores cadastradas"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink">
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} 
                                    style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog 
                header="Adicionando tutor" 
                maximizable={false} 
                visible={showModalForm} 
                style={{ width: '40vw' }} 
                onHide={closeModal}
            >
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-8">
                            <label htmlFor="nome" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" ref={register} defaultValue={tutor.nome}/>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="tipo_pessoa" className="form-control-label">Tipo de pessoa</label>
                            <select name="tipo_pessoa" className="form-control" name="tipo_pessoa" id="tipo_pessoa" ref={register} defaultValue={tutor.tipo_pessoa}>
                                <option value="" key="0" disabled selected>Selecione o tipo de pessoa</option>
                                <option value="Fisica">Física</option>
                                <option value="Juridica">Jurídica</option>
                            </select>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-6">
                            <label htmlFor="cpf_cnpj" className="form-control-label">CPF/CNPJ</label>
                            <input type="text" className="form-control" placeholder="cpf_cnpj" name="cpf_cnpj" ref={register} defaultValue={tutor.cpf_cnpj}/>
                        </div>
                        
                        <div className="col-md-6">
                            <label htmlFor="rg_ie" className="form-control-label">RG/IE</label>
                            <input type="text" className="form-control" placeholder="rg_ie" name="rg_ie" ref={register} defaultValue={tutor.rg_ie}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-4">
                            <label htmlFor="nacionalidade" className="form-control-label">Nacionalidade</label>
                            <input type="text" className="form-control" placeholder="nacionalidade" name="nacionalidade" ref={register} defaultValue={tutor.nacionalidade}/>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="sexo" className="form-control-label">Sexo</label>
                            <select name="sexo" className="form-control" name="sexo" id="sexo" ref={register} defaultValue={tutor.sexo}>
                                <option value="" key="0" disabled selected>Selecione o tipo de pessoa</option>
                                <option value="Masculino">Masculino</option>
                                <option value="Feminino">Feminino</option>
                            </select>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="data_nascimento" className="form-control-label">Data de nascimento</label>
                            <input type="date" className="form-control" placeholder="data_nascimento" name="data_nascimento" ref={register} defaultValue={tutor.data_nascimento}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-3">
                            <label htmlFor="cep" className="form-control-label">CEP</label>
                            <input type="text" className="form-control" placeholder="cep" name="cep" ref={register} defaultValue={tutor.cep}/>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="endereco" className="form-control-label">Endereço</label>
                            <input type="text" className="form-control" placeholder="endereco" name="endereco" ref={register} defaultValue={tutor.endereco}/>
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="numero" className="form-control-label">Número</label>
                            <input type="text" className="form-control" placeholder="numero" name="numero" ref={register} defaultValue={tutor.numero}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-6">
                            <label htmlFor="complemento" className="form-control-label">Complemento</label>
                            <input type="text" className="form-control" placeholder="complemento" name="complemento" ref={register} defaultValue={tutor.complemento}/>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="ponto_referencia" className="form-control-label">Ponto de referência</label>
                            <input type="text" className="form-control" placeholder="ponto de referência" name="ponto_referencia" ref={register} defaultValue={tutor.ponto_referencia}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-4">
                            <label htmlFor="bairro" className="form-control-label">Bairro</label>
                            <input type="text" className="form-control" placeholder="bairro" name="bairro" ref={register} defaultValue={tutor.bairro}/>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="cidade" className="form-control-label">Cidade</label>
                            <input type="text" className="form-control" placeholder="cidade" name="cidade" ref={register} defaultValue={tutor.cidade}/>
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="estado" className="form-control-label">Estado</label>
                            <select name="estado" className="form-control" name="estado" id="estado" ref={register} defaultValue={tutor.estado}>
                                <option value="" key="0" disabled selected>Selecione o estado</option>
                                {
                                    estados.map((estado) => {
                                        return <option key={estado.sigla} value={estado.sigla}>{estado.nome}</option>
                                    })
                                }
                            </select>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="observacao" className="form-control-label">Observações</label>
                            <textarea className="form-control" placeholder="Observações" ref={register} defaultValue={tutor.observacoes}>
                            </textarea>
                        </div>
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" ref={register} checked={tutor.status}/>
                        <label className="form-check-label" for="check">Ativo?</label>
                    </div>
                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>
                </form>
            </Dialog>
        </>
    );
}

export default Tutor