import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

import { Dialog } from 'primereact/dialog' 

import { FileUpload } from 'primereact/fileupload'

import Swal from 'sweetalert2';

//inputs
import {InputSwitch} from 'primereact/inputswitch' 
import { InputText } from 'primereact/inputtext' 
import { Dropdown } from 'primereact/dropdown' 


import { Button } from 'reactstrap' 

import { useForm } from "react-hook-form";

function Pacientes() {

    const [pacientes, setPacientes] = useState([])
    const [filter, setFilter] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [showModalConfirm, setShowModalConfirm] = useState(false)
    const [tutores, setTutores] = useState([])
    const [especies, setEspecies] = useState([])
    const [pelagens, setPelagens] = useState([])
    const [cores, setCores] = useState([])
    const [racas, setRacas] = useState([])
    const [fazendas, setFazendas] = useState([])
    const [paciente, setPaciente] = useState({
        id: '',
        nome: '',
        tutor_id: '',
        foto: '',
        data_nascimento: '',
        sexo: '',
        especie_id: '',
        raca_id: '',
        pelagem_id: '',
        cor_id: '',
        porte: '',
        castrado: '',
        microchipado: '',
        numero_chip: '',
        peso: '',
        fazenda_id: '',
        pratica_atividade_esportiva: '',
        atividade_esportiva: '',
        status: true
    })

    async function updateStatus() {
        //chamar api e alterar o status do tutor
        //alterar o estado do componente
    }

    const getPacientes = async () => {
        await api.get('/paciente').then(res => {
            const { data } = res;
            setPacientes(data);
        })
    }

    const getTutores = async () => {
        await api.get('/tutor').then(response => {
            const { data } = response;
            setTutores(data.rows)
        })
    }

    const getEspecies = async () => {
        await api.get('/especie').then(response => {
            const { data } = response;
            setEspecies(data.rows)
        })
    }

    const getCores = async () => {
        await api.get('/cor').then(response => {
            const { data } = response;
            setCores(data.rows)
        })
    }

    const getPelagens = async () => {
        await api.get('/pelagem').then(response => {
            const { data } = response;
            setPelagens(data.rows)
        })
    }

    const getRacas = async () => {
        await api.get('/raca').then(response => {
            const { data } = response;
            setRacas(data.rows)
        })
    }

    const getFazendas = async () => {
        await api.get('/fazenda').then(response => {
            const { data } = response;
            setFazendas(data.rows)
        })
    }

    useEffect(() => {
        getPacientes()
        getTutores()
        getEspecies()
        getCores()
        getPelagens()
        getRacas()
        getFazendas()
    },[])

    const openModalForm = (paciente = {}) => {
        if (!paciente.id) {
            setPaciente(prev => {return {...prev, id: '', nome: '' , tutor_id: '', 
            foto: '', data_nascimento: '', sexo: '', especie_id: '', raca_id: '', 
            pelagem_id: '', cor_id: '', porte: '', castrado: '', microchipado: '',
            numero_chip: '', peso: '', fazenda_id: '', pratica_atividade_esportiva: '',
            atividade_esportiva: '', status: true }}) 
            setShowModalForm(true)
            return
        }
        
        const { id, nome, foto, data_nascimento, sexo, especie_id, 
            raca_id, pelagem_id, cor_id, porte, castrado, microchipado, 
            numero_chip, peso, fazenda_id, pratica_atividade_esportiva, atividade_esportiva,  status } = paciente;

        setPaciente(prev => {return {...prev, id, nome ,foto, data_nascimento, sexo, especie_id, raca_id,
            pelagem_id, cor_id, porte, castrado, microchipado, numero_chip, peso, 
            fazenda_id, pratica_atividade_esportiva,
            atividade_esportiva, status: !!(status === 'Ativo') }})

        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {
        console.log('Paciente atual', rowData)

        let msg = (rowData.status==='Ativo') ?
            'Você está desativando o Paciente selecionado. <br>Gostaria de fazer isso?'
            : 'Você está ativando o Paciente selecionado. <br>Gostaria de fazer isso?';

            Swal.fire({
                title: 'Aviso!',
                html: msg,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Confirmar',
                confirmButtonColor: '#2dce89'
            }).then( async (result) => {
                console.log('res', result);

                let response;

                if(result.isConfirmed) {
                    response = await api.put(`/paciente/${rowData.id}`, {
                        nome: rowData.nome,
                        foto: rowData.foto,
                        status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                    })
        
                    console.log('res', response);
            
                    Swal.fire({
                        title: 'Sucesso!',
                        text: 'Status do paciente atualizado com sucesso.',
                        icon: 'success'
                    }).then(() => {
                        getPacientes();
                    })
                }
            }) 
    } 

    const closeModal = () => {
        setShowModalForm(false)
        setShowModalConfirm(false)
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
        {field: 'tutor_id', header: 'Tutor', sortable: 1},
        {field: 'fazenda_id', header: 'Fazenda', sortable: 1}
    ] 

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} 
        sortable={col.sortable} header={col.header}/> 
    })

    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} 
                    tooltip={(rowData.status==='Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                    openStatusHandler(rowData);
                }}/>
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : {'inactive' : true} 
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (paciente.id === '') {
            response = await api.post(`/paciente`, {
                nome: data.nome,
                tutor_id: data.tutor_id,
                foto: data.foto,
                data_nascimento: data.data_nascimento,
                sexo: data.sexo,
                especie_id: data.especie_id,
                raca_id: data.raca_id,
                pelagem_id: data.pelagem_id,
                cor_id: data.cor_id,
                porte: data.porte,
                castrado: data.castrado,
                microchipado: data.microchipado,
                numero_chip: data.numero_chip,
                peso: data.peso,
                fazenda_id: data.fazenda_id,
                pratica_atividade_esportiva: data.pratica_atividade_esportiva,
                atividade_esportiva: data.atividade_esportiva,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Paciente cadastrado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getPacientes();
            })
        } else {
            response = await api.put(`/paciente/${paciente.id}`, {
                nome: data.nome,
                tutor_id: data.tutor_id,
                foto: data.foto,
                data_nascimento: data.data_nascimento,
                sexo: data.sexo,
                especie_id: data.especie_id,
                raca_id: data.raca_id,
                pelagem_id: data.pelagem_id,
                cor_id: data.cor_id,
                porte: data.porte,
                castrado: data.castrado,
                microchipado: data.microchipado,
                numero_chip: data.numero_chip,
                peso: data.peso,
                fazenda_id: data.fazenda_id,
                pratica_atividade_esportiva: data.pratica_atividade_esportiva,
                atividade_esportiva: data.atividade_esportiva,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            console.log('res', response);
    
            Swal.fire({
                title: 'Sucesso!',
                text: 'Paciente atualizado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getPacientes();
            })
        }
    };

    console.log('watchAllFields', watchAllFields)

    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de pacientes</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar Paciente
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp; 
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>

                <div className="col text-right">
                    <InputText type="search" onInput={(e) => 
                      setFilter(e.target.value)} placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={pacientes} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há pacientes cadastrados"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink"
                            >
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} 
                                  style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog header="Adicionando paciente" maximizable={false} visible={showModalForm} style={{ width: '40vw' }} onHide={closeModal}>
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-9">
                            <label htmlFor="" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" ref={register} defaultValue={paciente.nome}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">

                        <div className="col-md-8">
                            <label htmlFor="" className="form-control-label">Sexo</label>
                            <select name="sexo" className="form-control" name="sexo" id="sexo" ref={register} defaultValue={''}>
                                <option value="" key="0" disabled selected>Selecione o Sexo</option>
                                <option key="macho" value="Femea">Fêmea</option>
                                <option key="femea" value="Macho">Macho</option>
                            </select>
                        </div>

                        <div className="col-md-4">
                            <label htmlFor="" className="form-control-label">Data de nascimento</label>
                            <input type="date" className="form-control" placeholder="Data" name="data" ref={register} defaultValue={paciente.data_nascimento}/>
                        </div>
   
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-9">
                            <label htmlFor="tutor" className="form-control-label">Tutor</label>

                            <select name="tutor" className="form-control" name="tutor" id="tutor" ref={register} defaultValue={paciente.tutor_id}>
                                <option value="" key="0" disabled selected>Selecione o tutor</option>
                                    {
                                        tutores.length > 0 && tutores.map((tutor) => {
                                        return <option key={tutor.id} value={tutor.id}>{tutor.nome}</option> })
                                    }
                            </select>
                        </div>

                        <div className="col-md-3">
                            <label htmlFor="cor" className="form-control-label">Cor</label>

                            <select name="cor" className="form-control" name="cor" id="cor" ref={register} defaultValue={paciente.cor_id}>
                                <option value="" key="0" disabled selected>Selecione a cor</option>
                                    {
                                        cores.length > 0 &&
                                        cores.map((cor) => {
                                        return <option key={cor.id} value={cor.id}>{cor.nome}</option> })
                                    }
                            </select>
                        </div>
                    </div>
                    
                    <div className="form-row mb-3">
                        <div className="col-md-4">
                            <label htmlFor="pelagem" className="form-control-label">Pelagem</label>

                            <select name="pelagem" className="form-control" name="pelagem" id="pelagem" ref={register} defaultValue={paciente.pelagem_id}>
                                <option value="" key="0" disabled selected>Selecione a pelagem</option>
                                    {
                                        pelagens.length > 0 && 
                                        pelagens.map((pelagem) => {
                                        return <option key={pelagem.id} value={pelagem.id}>{pelagem.nome}</option> })
                                    }
                            </select>
                        </div>

                        <div className="col-md-4">
                            <label htmlFor="raca" className="form-control-label">Raça</label>

                            <select name="raca" className="form-control" name="raca" id="raca" ref={register} defaultValue={paciente.raca_id}>
                                <option value="" key="0" disabled selected>Selecione a raça</option>
                                    {
                                        racas.length > 0 &&
                                        racas.map((raca) => {
                                        return <option key={raca.id} value={raca.id}>{raca.nome}</option> })
                                    }
                            </select>
                        </div>

                        <div className="col-md-4">
                            <label htmlFor="especie" className="form-control-label">Espécie</label>

                            <select name="especie" className="form-control" name="especie" id="especie" ref={register} defaultValue={paciente.especie_id}>
                                <option value="" key="0" disabled selected>Selecione a especie</option>
                                    {
                                        especies.length > 0 &&
                                        especies.map((especie) => {
                                        return <option key={especie.id} value={especie.id}>{especie.nome}</option> })
                                    }
                            </select>
                        </div>

                    </div>
                    
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="fazenda" className="form-control-label">Fazenda</label>

                            <select name="fazenda" className="form-control" name="fazenda" id="fazenda" ref={register} defaultValue={paciente.fazenda_id}>
                                <option value="" key="0" disabled selected>Selecione a fazenda</option>
                                    {
                                        fazendas.length > 0 &&
                                        fazendas.map((fazenda) => {
                                        return <option key={fazenda.id} value={fazenda.id}>{fazenda.nome}</option> })
                                    }
                            </select>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-3">
                            <label htmlFor="porte" className="form-control-label">Porte</label>
                            <select name="porte" className="form-control" name="porte" id="porte" ref={register} defaultValue={paciente.porte}>
                                <option value="" key="0" disabled selected>Selecione o Porte</option>
                                <option key="pequeno" value="Pequeno">Pequeno</option> 
                                <option key="medio" value="Médio">Médio</option>
                                <option key="grande" value="Grande">Grande</option>
                                <option key="gigante" value="Gigante">Gigante</option>
                            </select>
                        </div>

                        <div className="col-md-2">
                            <label htmlFor="castrado" className="form-control-label">Castrado?</label>
                            <select name="castrado" className="form-control" name="castrado" id="castrado" ref={register} defaultValue={paciente.castrado}>
                                <option value="" key="0" disabled selected>Castrado?</option>
                                <option key="sim" value="Sim">Sim</option> 
                                <option key="nao" value="Não">Não</option>
                            </select>         
                        </div>

                        <div className="col-md-3">
                            <label htmlFor="microchipado" className="form-control-label">Microchipado?</label>
                            <select name="microchipado" className="form-control" name="microchipado" id="microchipado" ref={register} defaultValue={paciente.microchipado}>
                                <option value="" key="0" disabled selected>Microchipado?</option>
                                <option key="sim" value="Sim">Sim</option> 
                                <option key="nao" value="Não">Não</option>
                            </select>   
                        </div>

                        <div className="col-md-4">
                            <label htmlFor="pratica_atividade_esportiva" className="form-control-label">Pratica Atividade Esportiva?</label>
                            <select name="pratica_atividade_esportiva" className="form-control" name="pratica_atividade_esportiva" id="pratica_atividade_esportiva" ref={register} defaultValue={paciente.pratica_atividade_esportiva}>
                                <option value="" key="0" disabled selected>Pratica atividade esportiva?</option>
                                <option key="sim" value="Sim">Sim</option> 
                                <option key="nao" value="Não">Não</option>
                            </select>   
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-3">
                            <label htmlFor="" className="form-control-label">Número do Chip</label>
                            <input type="text" className="form-control" placeholder="Número do chip" name="numero_chip" ref={register} defaultValue={paciente.numero_chip}/>
                        </div>

                        <div className="col-md-7">
                            <label htmlFor="" className="form-control-label">Atividade Esportiva</label>
                            <input type="text" className="form-control" placeholder="Atividade Esportiva" name="atividade_esportiva" ref={register} defaultValue={paciente.atividade_esportiva}/>
                        </div>

                        <div className="col-md-2">
                            <label htmlFor="" className="form-control-label">Peso</label>
                            <input type="number" className="form-control" placeholder="Peso" name="peso" ref={register} defaultValue={paciente.peso}/>
                        </div>
                    </div>

                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <FileUpload name="foto" accept="image/*" maxFileSize={1000000} mode="basic" chooseLabel="Escolher foto"/>
                        </div>
                    </div>

                    <div className="form-check">
                        <div className="col-md-3" style={{marginTop: '12px'}}>
                            <input type="checkbox" className="form-check-input" id="check" name="status" ref={register} checked={paciente.status}/>
                            <label className="form-check-label" for="check" >Ativo?</label>
                        </div>
                    </div>

                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>
                </form>
                
            </Dialog>

            <Dialog header="Deseja desativar o paciente?" visible={showModalConfirm} style={{ width: '20vw' }} onHide={closeModal}>
                <div className="text-center">
                    <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                    <Button 
                        className="btn btn-success" 
                        onClick={updateStatus}>
                        Confirmar
                    </Button>
                </div>
            </Dialog>
        </>
    )
}

export default Pacientes;