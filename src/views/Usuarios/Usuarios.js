import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

import {InputSwitch} from 'primereact/inputswitch';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Dialog } from 'primereact/dialog' 

import Swal from 'sweetalert2';

import { Button } from 'reactstrap';

import { useForm } from "react-hook-form";

function Usuarios() {

    const [usuarios, setTutores] = useState([])
    const [filter, setFilter] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false);
    const [modalHeaderText, setModalHeaderText] = useState('');

    const [usuario, setUsuario] = useState({
        id: '',
        nome: '',
        email: '',
        senha: '',
        status: true,
        administrador: false,
        cargo_id: ''
    });

    const editTutor = (tutor) => {
        alert(`Abrir modal para editar tutor ${tutor.nome}`)
    } 

    async function setTutorIsActive(tutor) {
        //chamar api e alterar o status do tutor
        //alterar o estado do componente
        return !tutor.status
    }

    const getUsuarios = async () => {
        await api.get('/usuario').then(res => {
            const { data } = res;
  
            console.log('lista de usuarios:', rows);
  
            setTutores(data.rows)
  
        })
    }

    useEffect(() => {
        getUsuarios();
    },[])

    const openModalForm = (usuario = {}) => {
        console.log('usuario:', usuario)
        
        if (!usuario.id) {
            setUsuario(prev => {return {...prev, id: '', nome: '' , email: '', senha: '', status: true, administrador: false, cargo_id: '' }});
            
            setModalHeaderText('Adicionando usuario');
            setShowModalForm(true);
            return
        }
        
        const { id, nome, email, senha, status, administrador, cargo_id } = usuario;

        setUsuario(prev => {return {...prev, id, nome , email, senha, status: !!(status==='Ativo'), administrador, cargo_id }})

        console.log('usuario alterado?', usuario)

        setModalHeaderText('Editando usuario');
        setShowModalForm(true)
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
        {field: 'email', header: 'E-mail', sortable: 1},
    ];

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} header={col.header}/>;
    });

    const adminStatusTranslateHandler = (rowData, column) =>{
        return (
            <div className="text-left">
                {rowData.administrador == true ? 'Sim' : 'Não'}
            </div>
        )
    }
    
    const actionEdit = (rowData, column) => {
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const openStatusHandler = (rowData) => {
        console.log('cargo atual', rowData)

        let msg = (rowData.status==='Ativo') ?
            'Você está desativando o usuario selecionado. <br>Gostaria de fazer isso?'
            : 'Você está ativando o usuario selecionado. <br>Gostaria de fazer isso?';

        if ((rowData.status==='Ativo')) {
            Swal.fire({
                title: 'Aviso!',
                html: msg,
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#dc3545',
                confirmButtonText: 'Confirmar',
                confirmButtonColor: '#2dce89'
            }).then( async (result) => {
                console.log('res', result);

                let response;

                if(result.isConfirmed) {
                    response = await api.put(`/usuario/${rowData.id}`, {
                        nome: rowData.nome,
                        descricao: rowData.descricao,
                        status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                    })
        
                    console.log('res', response);
            
                    Swal.fire({
                        title: 'Sucesso!',
                        text: 'Status do usuario atualizado com sucesso.',
                        icon: 'success'
                    }).then(() => {
                        getUsuarios();
                    })
                }

                // setShowModalForm(false);
                // getCargos();
            })
        }
        
    } 

    const closeModal = () => {
        setShowModalForm(false)
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} tooltip={(rowData.status==='Ativo') ? 'Desativar' : 'Ativar'} onChange={() => openStatusHandler(rowData)} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : {'inactive' : true};
    }

    const { register, handleSubmit, watch } = useForm();

    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (usuario.id === '') {
            response = await api.post(`/usuario`, {
                nome: data.nome,
                email: data.email,
                senha: data.senha,
                cargo_id: 3,
                descricao: data.descricao,
                status: data.status ? 'Ativo' : 'Inativo',
                administrador: data.administrador,
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Usuario cadastrado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getUsuarios();
            })
        } else {
            response = await api.put(`/usuario/${usuario.id}`, {
                nome: data.nome,
                email: data.email,
                senha: data.senha,
                cargo_id: 3,
                descricao: data.descricao,
                status: data.status ? 'Ativo' : 'Inativo',
                administrador: data.administrador,
            })

            console.log('res', response);
    
            Swal.fire({
                title: 'Sucesso!',
                text: 'Usuario atualizado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getUsuarios();
            })
        }


                
        console.log('teste', data)
        console.log('response', response);
        
    };

    console.log('watchAllFields', watchAllFields)

    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de usuarios</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar usuario
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp; 
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>
                <div className="col text-right">
                    <InputText type="search" onInput={(e) => setFilter(e.target.value)} placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={usuarios} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há usuarios cadastrados"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink"
                                lazy
                            >
                                {headers}
                                <Column header="Administrador" body={adminStatusTranslateHandler}/>
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog header={modalHeaderText} maximizable={false} visible={showModalForm} style={{ width: '40vw' }} onHide={closeModal}>
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" ref={register} defaultValue={usuario.nome}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">E-mail</label>
                            <input type="text" className="form-control" placeholder="E-mail" name="email" ref={register} defaultValue={usuario.email}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">Senha</label>
                            <input type="password" className="form-control" placeholder="Senha" name="senha" ref={register} defaultValue={usuario.senha}/>
                        </div>
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" ref={register} checked={usuario.status}/>
                        <label className="form-check-label" for="check">Ativo?</label>
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="administrador" ref={register} checked={usuario.administrador}/>
                        <label className="form-check-label" for="check">Administrador?</label>
                    </div>
                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>
                </form>
                
            </Dialog>
        </>
    );
}

export default Usuarios