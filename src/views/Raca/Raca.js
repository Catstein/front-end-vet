import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

//inputs
import {InputSwitch} from 'primereact/inputswitch';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import {Dialog} from 'primereact/dialog'


import { Button } from 'reactstrap';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';

function Raca() {

    const [filter, setFilter] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [especies, setEspecies] = useState([])
    const [racas, setRacas] = useState([])
    const [raca, setRaca] = useState({
        id: '',
        nome: '',
        especie_id: '',
        especie_nome: '',
        status: true
    })

    const getEspecies = async () => {
        api.get('/especie').then(res => {
            const { data } = res;

            setEspecies(data.rows);
        })
    }

    const getRacas = async () => {
        api.get('/raca').then(res => {
            const { data } = res;
            setRacas(data.rows);
        })
    }

    useEffect(() => {
        getEspecies()
        getRacas()
    },[])
    
    const openModalForm = (raca = {}) => {
        if (!raca.id) {
            setRaca(prev => {return {...prev, id: '', nome: '', especie_id: '', status: true }})
            setShowModalForm(true)
            return
        }
        
        const { id, nome, especie_id, status } = raca;

        setRaca(prev => {return {...prev, id, nome, especie_id, status: !!(status==='Ativo') }})


        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {

        let msg = (rowData.status==='Ativo') ?
            'Você está desativando a raça. <br>Gostaria de fazer isso?'
            : 'Você está ativando a raça. <br>Gostaria de fazer isso?';

        Swal.fire({
            title: 'Aviso!',
            html: msg,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            cancelButtonColor: '#dc3545',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#2dce89'
        }).then( async (result) => {

            let response;

            if(result.isConfirmed) {
                response = await api.put(`/raca/${rowData.id}`, {
                    nome: rowData.nome,
                    especie_id: rowData.especie_id,
                    status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                })
        
                Swal.fire({
                    title: 'Sucesso!',
                    text: 'Status da raça atualizada com sucesso.',
                    icon: 'success'
                }).then(() => {
                    getRacas();
                })
            }
        })
    } 

    const closeModal = () => {
        setShowModalForm(false)
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
        {field: 'especie_nome', header: 'Especie', sortable: 1},
    ];

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} 
            header={col.header}/>;
    });



    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} tooltip={(rowData.status==='Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                    openStatusHandler(rowData);
                }} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : 
            {'inactive' : true};
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        console.log(data)

        if (raca.id === '') {
            response = await api.post(`/raca`, {
                nome: data.nome,
                especie_id: data.especie_id,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Raça cadastrado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getRacas();
            })
        } else {
            response = await api.put(`/raca/${raca.id}`, {
                nome: data.nome,
                especie_id: data.especie_id,
                status: data.status ? 'Ativo' : 'Inativo'
            })

    
            Swal.fire({
                title: 'Sucesso!',
                text: 'Raça atualizado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getRacas();
            })
        }
        
    };


    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de raças</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar raça
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp;
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>

                <div className="col text-right">
                    <InputText type="search" onInput={(e) => 
                        setFilter(e.target.value)} 
                        placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={racas} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há raças cadastradas"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink">
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} 
                                    style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog 
                header="Adicionando raça" 
                maximizable={false} 
                visible={showModalForm} 
                style={{ width: '40vw' }} 
                onHide={closeModal}
            >
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="nome" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" ref={register} defaultValue={raca.nome}/>
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="especie" className="form-control-label">Especie</label>
                            <select name="especie" className="form-control" name="especie_id" ref={register} defaultValue={raca.especie_id}>
                                <option value="" key="0" disabled selected>Selecione a espécie</option>
                                {
                                    especies.length > 0 &&
                                    especies.map((especie) => <option key={especie.id} value={especie.id}>{especie.nome}</option> )
                                }
                            </select>
                        </div>
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" ref={register} checked={raca.status}/>
                        <label className="form-check-label" for="check">Ativo?</label>
                    </div>
                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>
                </form>
            </Dialog>
        </>
    );
}

export default Raca