import React, { useState, useEffect } from 'react'

import DefaultAdminHeader from "components/Headers/DefaultAdminHeader";

import api from '../../services/api'

import { DataTable, Column } from 'primereact/datatable'

//inputs
import {InputSwitch} from 'primereact/inputswitch';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import {Dialog} from 'primereact/dialog'


import { Button } from 'reactstrap';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';

// import '../clovis/styles.css'

function Area() {

    const [filter, setFilter] = useState()
    const [rows, setRows] = useState(10)
    const [showModalForm, setShowModalForm] = useState(false)
    const [areas, setAreas] = useState([])
    const [area, setArea] = useState({
        id: '',
        nome: '',
        status: true
    })

    const getAreas = async () => {
        await api.get('/areaatuacao').then(res => {
            const { data } = res;
    
            setAreas(data.rows);
        })
    }


    useEffect(() => {
        getAreas()
    },[])
    
    const openModalForm = (area = {}) => {
        if (!area.id) {
            setArea(prev => {return {...prev, id: '', nome: '' , status: true }})
            setShowModalForm(true)
            return
        }
        
        const { id, nome, status } = area;

        setArea(prev => {return {...prev, id, nome, status: !!(status==='Ativo') }})


        setShowModalForm(true)
    }

    const openStatusHandler = (rowData) => {

        let msg = (rowData.status==='Ativo') ?
            'Você está desativando a área selecionada. <br>Gostaria de fazer isso?'
            : 'Você está ativando o área selecionada. <br>Gostaria de fazer isso?';

        Swal.fire({
            title: 'Aviso!',
            html: msg,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            cancelButtonColor: '#dc3545',
            confirmButtonText: 'Confirmar',
            confirmButtonColor: '#2dce89'
        }).then( async (result) => {

            let response;

            if(result.isConfirmed) {
                response = await api.put(`/areaatuacao/${rowData.id}`, {
                    nome: rowData.nome,
                    status: (rowData.status==='Ativo') ? 'Inativo' : 'Ativo'
                })
        
                Swal.fire({
                    title: 'Sucesso!',
                    text: 'Status da área de atuação atualizada com sucesso.',
                    icon: 'success'
                }).then(() => {
                    getAreas();
                })
            }

            // setShowModalForm(false);
            // getCargos();
        })
        
    } 

    const closeModal = () => {
        setShowModalForm(false)
    }

    const cols = [
        {field: 'nome', header: 'Nome', sortable: 1},
    ];

    const headers = cols.map((col,i) => {
        return <Column key={col.field} field={col.field} sortable={col.sortable} 
            header={col.header}/>;
    });

    const actionEdit = (rowData, column) =>{
        return (
            <div className="text-left">
                <Button 
                    type="button" 
                    icon="pi pi-search" 
                    className="btn-edit"
                    onClick={() => openModalForm(rowData)}
                >
                    <i className="pi pi-user-edit p-mr-2"></i>
                </Button>
            </div>
        )
    }

    const actionDisable = (rowData, column) =>{
        return (
            <div>
                <InputSwitch checked={rowData.status==='Ativo'} tooltip={(rowData.status==='Ativo') ? 'Desativar' : 'Ativar'} onChange={() => {
                    openStatusHandler(rowData);
                }} />
            </div>
        )
    }

    const lineActive  = (rowData) => {
        return rowData.status === 'Ativo' ? {'inactive' : false} : 
            {'inactive' : true};
    }

    const { register, handleSubmit, watch } = useForm();
    const watchAllFields = watch();

    const onSubmit = async data => {
        let response;

        if (area.id === '') {
            response = await api.post(`/areaatuacao`, {
                nome: data.nome,
                status: data.status ? 'Ativo' : 'Inativo'
            })

            Swal.fire({
                title: 'Sucesso!',
                text: 'Área cadastrado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getAreas();
            })
        } else {
            response = await api.put(`/areaatuacao/${area.id}`, {
                nome: data.nome,
                status: data.status ? 'Ativo' : 'Inativo'
            })
    
            Swal.fire({
                title: 'Sucesso!',
                text: 'Área de atuação atualizado com sucesso.',
                icon: 'success'
            }).then(() => {
                setShowModalForm(false);
                getAreas();
            })
        }
        
    };

    const headerTable = (
        <div className="card-header py-2 px-0">
            <div className="row align-items-center custom-divider mx-0 pb-2">
                <h3 className="col-md-3 title m-0 text-left">Lista de Áreas de Atuação</h3>
                <div className="col-md text-right">
                    <Button type="button" className="btn btn-padrao" onClick={() => openModalForm()}>
                        + Adicionar Area de atuação
                    </Button>
                </div>
            </div>
            <div className="row mx-0 pt-2">
                <div className="col text-left">
                    Mostrar:&nbsp;
                    <Dropdown 
                        value={rows}
                        options={[
                            {label: 10, value: 10},
                            {label: 20, value: 20},
                            {label: 30, value: 30},
                            {label: 50, value: 50},
                            {label: 100, value: 100}
                        ]} 
                        onChange={(e) => {setRows(e.value)}}
                    />
                </div>

                <div className="col text-right">
                    <InputText type="search" onInput={(e) => 
                        setFilter(e.target.value)} 
                        placeholder="Pesquisar" size="20"/>
                </div>
            </div>
        </div>
    );

    return (
        <>
            <DefaultAdminHeader/>
            <div className="container-fluid mt--7">
                <div className="row">
                    <div className="col">
                        <div className="card border-none shadow">
                            <DataTable 
                                value={areas} 
                                paginator={true} 
                                rows={rows} 
                                responsive={true} 
                                header={headerTable} 
                                globalFilter={filter}
                                rowClassName={lineActive}
                                emptyMessage="Não há áreas cadastradas"
                                paginatorTemplate="PrevPageLink PageLinks NextPageLink">
                                {headers}
                                <Column body={actionEdit} style={{width: '5em'}}/>
                                <Column header="Ativo" body={actionDisable} 
                                    style={{textAlign:'center', width: '6em'}}/>
                            </DataTable>
                        </div>
                    </div>
                </div>
            </div>

            <Dialog 
                header="Adicionando área de atuação" 
                maximizable={false} 
                visible={showModalForm} 
                style={{ width: '40vw' }} 
                onHide={closeModal}
            >
                <form action="" onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <label htmlFor="" className="form-control-label">Nome</label>
                            <input type="text" className="form-control" placeholder="Nome" name="nome" ref={register} defaultValue={area.nome}/>
                        </div>
                    </div>
                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="check" name="status" ref={register} checked={area.status}/>
                        <label className="form-check-label" for="check">Ativo?</label>
                    </div>
                    <div className="text-right">
                        <Button className="btn btn-danger" onClick={closeModal}>Cancelar</Button>
                        <Button 
                            className="btn btn-success" 
                            type="submit"
                        >
                            Confirmar
                        </Button>
                    </div>
                </form>
            </Dialog>
        </>
    );
}

export default Area